FROM tomcat:9-jre11-slim

ARG deployable_artifact

COPY $deployable_artifact /usr/local/tomcat/webapps/